using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
class Solution {

    
    static int[] arrSorted(int[] numbers)
    {
        int min = numbers[0];
        int temp = 0;
        bool change = false;

        do
        {
            change = false;

            for (int i = 1; i < numbers.Length; i++)
            {
                if (numbers[i - 1] > numbers[i])
                {
                    temp = numbers[i];
                    numbers[i] = numbers[i - 1];
                    numbers[i - 1] = temp;
                    change = true;
                }
            }

        } while (change);        

        return numbers;
    }

    static int getmediam(string numbers)
    {
        string[] arr_numbers_temp = numbers.Split(' ');
        int[] arr_numbers = Array.ConvertAll(arr_numbers_temp, Int32.Parse);
        int pos = 0;
        
        //sort
        arr_numbers = arrSorted(arr_numbers);

        //mediam
        if(arr_numbers.Length % 2 == 0)
        {
            pos = Convert.ToInt32(arr_numbers.Length / 2);
            int numberA = arr_numbers[pos];
            int numberB = arr_numbers[pos - 1];
            decimal temp = Math.Round(Convert.ToDecimal((numberA + numberB) / 2));
            return Convert.ToInt32(temp);
        }
        
        pos = Convert.ToInt32(arr_numbers.Length / 2);
        return arr_numbers[pos];        
    }

    static void meadians(int[,] matrix, int[,] querys, int querys_amount)
    {
        int mediam = 0;
        int[] result = {};
        string numbers = "";        

        for (int i = 0; i < querys_amount; i++)
        {
            numbers = "";
            
            //range
            int c1 = querys[i, 0];
            int r1 = querys[i, 1];
            int c2 = querys[i, 2];
            int r2 = querys[i, 3];
            
            for (int c = c1; c <= c2; c++)
            {
                for (int r = r1; r <= r2; r++)
                {
                    //add
                    if(numbers.Length > 0)
                        numbers += " ";

                    numbers += matrix[c-1, r-1];
                }
            }

            //media
            mediam = getmediam(numbers);
            Console.WriteLine(mediam);
        }
        
    }

    static void Main(String[] args)
    {
        int n = Convert.ToInt32(Console.ReadLine());
        int[,] matrix = new int[n, n];
        
        for (int i = 0; i < n; i++)
        {
            String str = Console.ReadLine();
            String[] strArr = str.Split(' ');

            for (int j = 0; j < strArr.Length; j++)
            {
                matrix[i, j] = Convert.ToInt32(strArr[j]);
            }
        }

        int q = Convert.ToInt32(Console.ReadLine());
        int[,] querys= new int[q, 4];

        for (int i = 0; i < q; i++)
        {
            String str = Console.ReadLine();
            String[] strArr = str.Split(' ');

            for (int j = 0; j < strArr.Length; j++)
            {
                querys[i, j] = Convert.ToInt32(strArr[j]);
            }
        }

        meadians(matrix, querys, 3);
        Console.ReadLine();
    }
}
